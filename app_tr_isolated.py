from src.thread_isolated.storage import Storage
from src.thread_isolated.thread_executable import OperationHandler, ThreadExecutor
from src.thread_isolated.eventhandler import EventHandler

from threading import Thread

storage = Storage()
events = EventHandler()
handler1 = OperationHandler(storage, eventhandler=events)
handler2 = OperationHandler(storage, eventhandler=events)


print(f"Initial storage: {storage.storage}")

thread1 = ThreadExecutor(target=handler1.run, name="thread1")
thread2 = ThreadExecutor(target=handler2.run, name="thread2")

events.on('call time', thread1.scheduleOnThread(handler1.callable_func))
events.on('call time', thread2.scheduleOnThread(handler2.callable_func))
events.on('call time', storage.call_all)

thread1.start()
thread2.start()
print(f"Storage: {storage.storage}")

print("call time!")

events.emit("call time")

thread1.join()
thread2.join()

print(f"Final storage: {storage.storage}")