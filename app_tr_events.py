from src.thread_isolated.storage import Storage
from src.thread_isolated.thread_executable import OperationHandler,ThreadedOperationHandler, ThreadExecutor
from src.thread_isolated.eventhandler import EventHandler, ThreadEventHandler

from threading import Thread

storage = Storage()
events = EventHandler()
handler1 = ThreadedOperationHandler(storage, eventhandler=events)
handler2 = ThreadedOperationHandler(storage, eventhandler=events)


print(f"Initial storage: {storage.storage}")

# thread1 = ThreadExecutor(target=handler1.run, name="thread1")
# thread2 = ThreadExecutor(target=handler2.run, name="thread2")

# events.on('call time', thread1.scheduleOnThread(handler1.callable_func))
# events.on('call time', thread2.scheduleOnThread(handler2.callable_func))
#events.on('call time', storage.call_all)

handler1.start()
handler2.start()
print(f"Storage: {storage.storage}")

print("call time!")

events.emit("call time")

handler1.wait_for_completion()
handler2.wait_for_completion()

print(f"Final storage: {storage.storage}")