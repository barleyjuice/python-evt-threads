from src.storage import Storage
from src.thread_executable import OperationHandler
from src.eventhandler import EventHandler

from threading import Thread

storage = Storage()
events = EventHandler()
handler1 = OperationHandler(storage, eventhandler=events)
handler2 = OperationHandler(storage, eventhandler=events)


# events.on('call time', handler1.callable_func)
# events.on('call time', handler2.callable_func)
events.on('call time', storage.call_all)

print(f"Initial storage: {storage.storage}")

thread1 = Thread(target=handler1.run, name="thread1")
thread2 = Thread(target=handler2.run, name="thread2")

thread1.start()
thread2.start()
print(f"Storage: {storage.storage}")

events.emit("call time")

thread1.join()
thread2.join()

print(f"Final storage: {storage.storage}")