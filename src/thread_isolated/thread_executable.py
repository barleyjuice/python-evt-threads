import threading, queue

import time

class ThreadExecutor(threading.Thread):
    def __init__(self, target, name, loop_time = 1.0/60) -> None:
        self.q = queue.Queue()
        self.timeout = loop_time
        super().__init__(target=target, name=name)

    def onThread(self, function, *args, **kwargs):
        self.q.put((function, args, kwargs)) 

    def scheduleOnThread(self, function):
        def schedule():
            self.onThread(function=function)
        return schedule

    def run(self):
        super().run()
        print("Starting eventloop")
        while True:
            try:
                function, args, kwargs = self.q.get(timeout=self.timeout)
                function(*args, **kwargs)
            except queue.Empty:
                break
            except (KeyboardInterrupt, SystemExit):
                break
                #time.sleep(1)
                #self.idle()

def current_thread(fn):
    def threaded_fn():
        fn()
    return threaded_fn

class OperationHandler:
    def __init__(self, storage, eventhandler) -> None:
        self.storage = storage
        self.eventhandler = eventhandler
        print(f"Created object with id {id(self)}")

    def run(self):
        print(f"Running in thread {threading.current_thread().name}")
        self.eventhandler.on('call time', self.callable_func)
        self.storage.add_item(self.callable_func)

    
    def callable_func(self):
        print(f'Called from object {id(self)}, thread {threading.current_thread().name}')


class ThreadedOperationHandler(OperationHandler):
    def __init__(self, storage, eventhandler) -> None:
        self.thread = None
        super().__init__(storage, eventhandler)

    def callable_func(self):
        print("calling function from thread wrapper")
        #super().callable_func()
        self.thread.onThread(super().callable_func)

    def start(self):
        self.thread = ThreadExecutor(target=self.run, name=f"thread_{id(self)}")
        self.thread.start()

    def wait_for_completion(self):
        self.thread.join()