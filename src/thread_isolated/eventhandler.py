from threading import Event

class EventHandler:
    def __init__(self) -> None:
        self.listeners = {}

    def on(self, event, handler):
        if not (event in self.listeners):
            self.listeners[event] = []

        self.listeners[event].append(handler)

    def emit(self, event):
        if event in self.listeners:
            for listener in self.listeners[event]:
                listener()

# class ThreadEventHandler(EventHandler):
#     def __init__(self) -> None:
#         self.events = {}
#         super().__init__()

#     def on(self, event, handler):
#         if not (event in self.events):
#             self.events[event] = Event()

#     def emit(self, event):
#         if event in self.events:
#             self.events[event].set()
        