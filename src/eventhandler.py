
class EventHandler:
    def __init__(self) -> None:
        self.listeners = {}

    def on(self, event, handler):
        if not (event in self.listeners):
            self.listeners[event] = []

        self.listeners[event].append(handler)

    def emit(self, event):
        if event in self.listeners:
            for listener in self.listeners[event]:
                listener()