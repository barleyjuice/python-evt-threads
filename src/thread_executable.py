import threading


class OperationHandler:
    def __init__(self, storage, eventhandler) -> None:
        self.storage = storage
        self.eventhandler = eventhandler
        print(f"Created object with id {id(self)}")

    def run(self):
        print(f"Running in thread {threading.current_thread().name}")
        self.eventhandler.on('call time', self.callable_func)
        self.storage.add_item(self.callable_func)

    def callable_func(self):
        print(f'Called from object {id(self)}, thread {threading.current_thread().name}')