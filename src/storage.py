class Storage:

    def __init__(self) -> None:
        self.storage = []
        print(f"Initialized storage with id {id(self)}")

    def add_item(self, item):
        self.storage.append(item)

    def call_item(self, id):
        try:
            self.storage[id]()
        except Exception as e:
            print(e)

    def call_all(self):
        for item in self.storage:
            try:
                print("Calling from storage")
                item()
            except Exception as e:
                print(e)